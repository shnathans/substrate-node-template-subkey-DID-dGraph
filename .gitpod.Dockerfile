FROM dgraph/dgraph

USER gitpod

# Install custom tools, runtime, etc. using apt-get
# For example, the command below would install "bastet" - a command line tetris clone:
#
#RUN sudo curl https://get.dgraph.io -sSf | bash*
#
# More information: https://www.gitpod.io/docs/config-docker/
